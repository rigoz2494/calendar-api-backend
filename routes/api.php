<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@authenticate');

//Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('logout', 'UserController@logout');

    /* Users */
    Route::group(['prefix' => 'user'], function () {
        Route::get('/', 'UserController@getAuthenticatedUser');
    });

    /* Organisations */
    Route::get('organisation/{url}', 'OrganisationController@show');
    Route::apiResource('organisation', 'OrganisationController')->except(['show']);

    /* Calendars */
    Route::get('calendar/trash', 'CalendarController@trash');
    Route::apiResource('calendar', 'CalendarController');

    /* Events */
    Route::post('event/store', 'EventController@store');

    /* Devices */
    Route::get('devices/get-types', 'DevicesController@getTypes');
    Route::apiResource('devices', 'DevicesController');
    Route::apiResource('devices/types', 'DevicesTypesController');

    /* Categories */
    Route::get('categories/get-categories', 'CategoryController@getCategories');
    Route::apiResource('categories', 'CategoryController');
//});
