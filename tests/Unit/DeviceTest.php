<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\Device;
use App\Models\DeviceType;
use App\Models\Organisation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\Facades\JWTAuth;

class DeviceTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(User::class)->create();
        Auth::loginUsingId($user->id);
        $category = factory(Category::class)->create();
        $organisation = factory(Organisation::class)->create([
            'name' => $user->name,
            'category_id' => $category->id
        ]);

        $deviceType = factory(DeviceType::class)->create();
        $token = JWTAuth::fromUser(Auth::user());

        $data = [
            'description' => "",
            'device_type_id' => $deviceType->id,
            'diagonal' => "66",
            'resolution' => "",
            'organisation_id' => 2,
        ];
        /* Create Organisation */
        $deviseStoreResponse = $this->json('POST', '/api/devices', $data,[
            'Authorization' => 'bearer '. $token
        ]);

        $deviseStoreResponse->assertStatus(201)->assertSeeText('created');
    }
}
