<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\Organisation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use function Psy\sh;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\Facades\JWTAuth;

class OrganisationTest extends TestCase
{

    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testOrganisationCrud()
    {
        $user = factory(User::class)->create();
        Auth::loginUsingId($user->id);
        $category = factory(Category::class)->create();

        /* Bearer token*/
        $token = JWTAuth::fromUser(Auth::user());

        $week = [
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 1, 'closed' => true, 'all_day' => true],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 2, 'closed' => true, 'all_day' => true],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 3, 'closed' => true, 'all_day' => false],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 4, 'closed' => true, 'all_day' => false],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 5, 'closed' => true, 'all_day' => false],
            ['open_at' => null, 'close_at' => null, 'day_of_week' => 6, 'closed' => true, 'all_day' => false],
            ['open_at' => null, 'close_at' => null, 'day_of_week' => 7, 'closed' => true, 'all_day' => false],
        ];

        /* Organisation create */
        $create = $this->json('POST', '/api/organisation/', [
            'organisation' => [
                'name' => rand(0, 9999),
                'icon' => 'icon',
                'url' => 'url',
                'category_id' => $category->id
            ],
            'week' => $week
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $create->assertStatus(201);
        $organisation = Organisation::all()->first();

        /* Organisation update */
        $update = $this->json('PUT', '/api/organisation/'.$organisation->id, [
            'name'  => 'updated',
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $update->assertStatus(200);

        /* Organisation show */
        $show = $this->json('GET', '/api/organisation/'.$organisation->url, [
            'name'  => 'updated',
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $show->assertStatus(200);

        /* Organisation delete */
        $delete = $this->json('delete', '/api/organisation/'.$organisation->id, [],[
            'Authorization' => 'bearer '. $token
        ]);

        $delete->assertStatus(204);
    }
}
