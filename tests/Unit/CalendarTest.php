<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\Organisation;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tymon\JWTAuth\Facades\JWTAuth;

class CalendarTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCalendarCrud()
    {
        $user = factory(User::class)->create();
        Auth::loginUsingId($user->id);
        $category = factory(Category::class)->create();

        $week = [
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 1, 'closed' => true, 'all_day' => true],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 2, 'closed' => true, 'all_day' => true],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 3, 'closed' => true, 'all_day' => false],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 4, 'closed' => true, 'all_day' => false],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 5, 'closed' => true, 'all_day' => false],
            ['open_at' => null, 'close_at' => null, 'day_of_week' => 6, 'closed' => true, 'all_day' => false],
            ['open_at' => null, 'close_at' => null, 'day_of_week' => 7, 'closed' => true, 'all_day' => false],
        ];

        /* Bearer token*/
        $token = JWTAuth::fromUser(Auth::user());

        /* Create Organisation */
        $organisation = $this->json('POST', '/api/organisation/', [
            'organisation' => [
                'name' => rand(0, 9999),
                'icon' => 'icon',
                'url' => 'url',
                'category_id' => $category->id
            ],
            'week' => $week
        ],[
            'Authorization' => 'bearer '. $token
        ]);
        $organisation_id = Organisation::all()->first()->id;

        /* Calendar create */
        $create = $this->json('POST', '/api/calendar/', [
            'name'  => rand(0, 9999),
            'organisation_id' => $organisation_id
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $create->assertStatus(201);
        $id = $create->decodeResponseJson('calendar')['id'];

        /* Calendar update */
        $update = $this->json('PUT', '/api/calendar/'.$id, [
            'name'  => 'updated',
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $update->assertStatus(200);

        /* Calendar show */
        $show = $this->json('GET', '/api/calendar/'.$id, [
            'name'  => 'updated',
        ],[
            'Authorization' => 'bearer '. $token
        ]);
        $show->assertStatus(200);

        /* Calendar delete */
        $delete = $this->json('delete', '/api/calendar/'.$id, [],[
            'Authorization' => 'bearer '. $token
        ]);

        $delete->assertStatus(204);

        /* Trash */
        $trash = $this->json('GET', '/api/calendar/trash', [
            'organisation_id' => $organisation_id
        ],[
            'Authorization' => 'bearer '. $token
        ]);
    }
}
