<?php

namespace Tests\Unit;

use App\Models\Category;
use App\Models\Organisation;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $error = false;
        $user = factory(User::class)->create();
        $category = factory(Category::class)->create();
        $week = [
            ['open_at' => "00:00:00", 'close_at' => "00:00:00", 'day_of_week' => 1, 'closed' => true, 'all_day' => true],
            ['open_at' => "00:00:00", 'close_at' => "00:00:00", 'day_of_week' => 2, 'closed' => true, 'all_day' => true],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 3, 'closed' => true, 'all_day' => false],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 4, 'closed' => true, 'all_day' => false],
            ['open_at' => "08:00:00", 'close_at' => "15:00:00", 'day_of_week' => 5, 'closed' => true, 'all_day' => false],
            ['open_at' => null, 'close_at' => null, 'day_of_week' => 6, 'closed' => true, 'all_day' => false],
            ['open_at' => null, 'close_at' => null, 'day_of_week' => 7, 'closed' => true, 'all_day' => false],
        ];
        Auth::loginUsingId($user->id);

        /* Bearer token*/
        $token = JWTAuth::fromUser(Auth::user());
        /* Organisation create */
        $newOrg = $this->json('POST', '/api/organisation/', [
            'organisation' => [
                'name' => rand(0, 9999),
                'icon' => 'icon',
                'url' => 'url',
                'category_id' => $category->id
            ],
            'week' => $week
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $newOrg->assertSeeText('Organisation Created');

        $organisation_id = Organisation::all()->first()->id;

        /* Calendar create */
        $calendar = $this->json('POST', '/api/calendar/', [
            'name'  => 'calendar',
            'organisation_id' => $organisation_id
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $id = $calendar->decodeResponseJson('calendar')['id'];

        /* Event create */
        $res = $this->json('POST', '/api/event/store', [
            'name'  => 'event',
            'calendar_id' => $id,
            'event' => [
                'name' => uniqid('eventName_'),
                'files' => [
                    UploadedFile::fake()->image('file_1.jpg'),
                    UploadedFile::fake()->image('file_2.jpg'),
                    UploadedFile::fake()->create('document.pdf', 2000)
                ]
            ]
        ],[
            'Authorization' => 'bearer '. $token
        ]);

        $files_path = $res->decodeResponseJson('files');

        foreach ($files_path as $path) {
            if (!File::exists($path)) {
                $error = true;;
            }
            File::delete($path);
        }

        if ($error) {
            $this->fail("File not exist!");
        }

        $res->assertStatus(201);
    }
}
