<?php

namespace Tests\Unit;

use App\Models\User;
use Symfony\Component\VarDumper\Dumper\DataDumperInterface;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;

class UserTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testRegistrationAndLogin()
    {
        \App\Models\Category::create([
            'name' => 'other',
            'description' => 'other'
        ]);

        /* Test Data */
        $data = [
            'email'     =>  'gefira@mail.ch',
            'name'      =>  'Vladimir S.',
            'password'  =>  'secret11'
        ];
        /* Registration request */
        $registrationResponse = $this->json('POST', '/api/register', [
            'email' => $data['email'],
            'name'  => $data['name'],
            'password' => $data['password'],
            'password_confirmation' => $data['password'],
        ]);

        /* Registration response */
        $registrationResponse
            ->assertStatus(201)
            ->assertJsonStructure([
                'token', 'user'
            ])
        ;

        /* Bearer token*/
        $bearerToken = $registrationResponse->decodeResponseJson('token');

        /* Login request */
        $loginResponse = $this->json('POST', '/api/login', [
            'email' => $data['email'],
            'password' => $data['password'],
        ],[
            'Authorization' => 'bearer '. $bearerToken
        ]);

        /* Login response */
        $loginResponse
            ->assertStatus(200)
            ->assertJsonStructure([
                'token'
            ])
        ;

//        $this->json('GET', '/api/user', [
//            'email' => $data['email'],
//            'password' => $data['password'],
//        ],[
//            'Authorization' => 'bearer '. $bearerToken
//        ]);

        /* Check if Autheticated */
        $user = User::where('email', $data['email'])->first();

        if(!$this->assertAuthenticatedAs($user)){
            $this->fail("Not authenticated");
        }

        $this->assertTrue(true);
    }

    public function testRegistrationWithUncorrectedData()
    {
        $data = [
            'email'     =>  'gefira@mail.ch',
            'name'      =>  'Vladimir S.',
            'password'  =>  'secret11',
            'wrong_pass'=>  'wrong_pass'
        ];

        $response = $this->json('POST', '/api/register', [
            'email' => $data['email'],
            'name'  => $data['name'],
            'password' => $data['password'],
            'password_confirmation' => $data['wrong_pass'],
        ]);

        $response
            ->assertStatus(400)
        ;
    }
}
