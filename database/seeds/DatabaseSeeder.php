<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(DevicesSeeder::class);
//        $this->call(FakeSeeder::class);
//        $this->call(DimaSeeder::class);
        $this->call(ChatSeeder::class);
    }
}
