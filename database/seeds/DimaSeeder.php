<?php

use Illuminate\Database\Seeder;

class DimaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\Models\User::create([
            'email' => 'dimchikpc@gmail.com',
            'name'  => 'Dmitry Dorofeev',
            'password' => \Illuminate\Support\Facades\Hash::make('volumepower')
        ]);

        $category = factory(\App\Models\Category::class)->create();

        $organisation = factory(\App\Models\Organisation::class)->create([
            'name' => $user->name,
            'category_id' => $category->id,
            'icon'  => 'dashboard'
        ]);
        $calendar = $organisation->calendars()->create([
            'name' => uniqid('calendar_')
        ]);

        \Illuminate\Support\Facades\DB::table('organisations_users')
            ->insert([
                'user_id' => $user->id,
                'organisation_id' => $organisation->id,
                'is_owner' => true
            ]);

        factory(\App\Models\Event::class, 5)->create([
            'calendar_id' => $calendar->id
        ])->each(function ($event){
            factory(\App\Models\Attachment::class, 5)->create([
                'event_id' => $event->id
            ]);
        });

        for ($i = 1; $i <= 7; $i++)
        {
            $day = factory(\App\Models\Day::class)->create(
                [
                    'open_at' => "08:00:00",
                    'close_at' => "17:00:00",
                    'organisation_id' => $organisation->id,
                    'day_of_week' => $i
                ]);

            if ($i == 4) {
                factory(\App\Models\Exception::class)->create([
                    'day_id' => $day->id,
                    'open_at' => "09:00:00"
                ]);
            }

            if (in_array($i, [6,7])) {
                factory(\App\Models\Day::class)->create(
                    [
                        'organisation_id' => $organisation->id,
                        'day_of_week' => $i,
                        'open_at' => null,
                        'close_at' => null,
                    ]);
            }
        }
    }
}
