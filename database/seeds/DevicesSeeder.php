<?php

use Illuminate\Database\Seeder;

class DevicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'TV', 'description' => 'TV desc'],
            ['name' => 'Monitor', 'description' => 'monitore desc'],
            ['name' => 'Projector', 'description' => 'project desc'],
            ['name' => 'Other', 'description' => 'other desc'],
        ];

        \DB::table('devices_type')->insert($data);
    }
}
