<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Organisation::class, function (Faker $faker) {
    return [
        'url'   =>  uniqid(),
        'address' => $faker->address
    ];
});
