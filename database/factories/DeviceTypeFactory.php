<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\DeviceType::class, function (Faker $faker) {
    return [
        'name' => uniqid('name_'),
        'description' => uniqid('description_')
    ];
});
