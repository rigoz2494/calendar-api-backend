<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Device::class, function (Faker $faker) {
    return [
        'type' => uniqid('type_'),
        'diagonal' => rand(10, 30),
        'resolution' => 'resolution',
        'description' => uniqid('description_'),
    ];
});
