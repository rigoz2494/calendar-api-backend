<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Exception::class, function (Faker $faker) {
    return [
        'open_at' => "09:00:00",
        'close_at' => "16:00:00",
        'date' => \Carbon\Carbon::now()->toDateString(),
    ];
});
