<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Day::class, function (Faker $faker) {
    return [
        'open_at' => "08:00:00",
        'close_at' => "17:00:00",
    ];
});
