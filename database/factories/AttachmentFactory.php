<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Attachment::class, function (Faker $faker) {
    return [
        'file_name' => uniqid('fileName_'),
        'file_path' => storage_path(),
        'file_size' => rand(1000, 100000),
        'file_extension' => $faker->fileExtension,
    ];
});
