<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = ['name'];

    public function attached()
    {
        return $this->hasMany('App\Models\Attachment');
    }
}
