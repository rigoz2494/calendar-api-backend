<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends Model
{
    use SoftDeletes;

    protected $fillable = ['name'];

    public function events()
    {
        return $this->hasMany('App\Models\Event');
    }

    protected $dates = ['deleted_at'];
}
