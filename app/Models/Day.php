<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{
    protected $fillable = ['open_at', 'close_at', 'day_of_week'];
    public $timestamps = false;

    public function exceptions()
    {
        return $this->hasMany('App\Models\Exception');
    }
}
