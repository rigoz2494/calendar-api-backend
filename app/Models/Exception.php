<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exception extends Model
{
    protected $fillable = ['date', 'open_at', 'close_at'];

    public $timestamps = false;
}
