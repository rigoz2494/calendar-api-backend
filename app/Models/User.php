<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function organisations()
    {
        return $this->belongsToMany(
            'App\Models\Organisation',
            'organisations_users',
            'user_id',
            'organisation_id'
        );
    }


//    todo! for the future: node, socket io, brodcast
//    public function chats()
//    {
//        return $this->belongsToMany('App\Models\Chats', 'user_chat', 'user_id', 'chat_id');
//    }
}
