<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organisation extends Model
{
    use SoftDeletes;

    protected $fillable = ['name', 'address', 'icon', 'url', 'category_id'];

    public function calendars()
    {
        return $this->hasMany('App\Models\Calendar');
    }

    public function days()
    {
        return $this->hasMany('App\Models\Day')->with('exceptions');
    }

    public function devices()
    {
        return $this->hasMany('App\Models\Device');
    }

    public function daysWithExceptions()
    {
        return $this->hasManyThrough(
            'App\Models\Exception',
            'App\Models\Day',
            'organisation_id', // Foreign key on exception table...
            'day_id', // Foreign key on day table...
            'id',
            'id'
        );
    }

    protected $dates = ['deleted_at'];
}
