<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceType extends Model
{
    public $timestamps = false;

    protected $fillable = ['name', 'description'];

    protected $table = 'devices_type';
}
