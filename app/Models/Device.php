<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = ['diagonal', 'device_type_id', 'resolution', 'description', 'organisation_id'];
    protected $hidden = ['device_type_id'];

    public function organisation()
    {
        return $this->belongsTo('App\Models\Organisation');
    }

    public function deviceType()
    {
        return $this->belongsTo('App\Models\DeviceType');
    }
}
