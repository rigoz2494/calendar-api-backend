<?php

namespace App\Http\Controllers;

use App\Models\DeviceType;
use App\Models\Organisation;
use Illuminate\Http\Request;

class DevicesController extends Controller
{
    public function getTypes()
    {
        $types = DeviceType::all();
        return response()->json($types);
    }

    public function store(Request $request)
    {
        $organisation = Organisation::where('id', $request->get('organisation_id'))->first();
        $organisation->devices()->create($request->all());

        return response()->json(['message' => 'created'], 201);
    }
}
