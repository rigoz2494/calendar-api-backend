<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrganisationResource;
use App\Models\Organisation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrganisationController extends Controller
{

    public function index()
    {
        return 'Index';
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'url' => 'required|unique:organisation,url'
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors' => $validator->errors()], 422);
        }

        $organisation = Auth::user()->organisations()->create($request->get('organisation'));
        foreach ($request->get('week') as $day) {
            /* Check if day working all Day or Closed  */
            $check = $this->checkDay($day);

            $organisation->days()->create($check);
        }

        return response()->json(['message' => 'Organisation Created'], 201);
    }

    public function show($url)
    {
        $organisation = Organisation::where('url', $url)->firstOrFail();

        $info = new OrganisationResource($organisation);
        $devices = $organisation->devices()->with('deviceType')->get();

        return response()->json(compact('devices', 'info'));
    }

    /* Update organisation */
    public function update(Request $request, $id)
    {
        /* Check if User got organisation with id */
        $organisation = Organisation::where('id', $id)->first();
        if (!$organisation) {
            return response()->json(['message' => 'Organisation not found'], 404);
        }

        $organisation->update($request->all());
        return response()->json(['message' => 'Organisation update successful'], 200);
    }

    public function destroy($id)
    {
        $organisation = Organisation::Find($id);
        $organisation->delete();

        return response()->json(['message' => 'Organisation deleted successfully'], 204);
    }

    private function checkDay($day)
    {
        if ($day['all_day']) {
            $day['open_at'] = "00:00:00";
            $day['close_at'] = "00:00:00";
        }
        if ($day['closed']) {
            $day['open_at'] = null;
            $day['close_at'] = null;
        }

        return $day;
    }
}
