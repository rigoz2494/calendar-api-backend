<?php

namespace App\Http\Controllers;

use App\Http\Resources\CalendarsResource;
use App\Models\Calendar;
use App\Models\Organisation;
use Illuminate\Http\Request;

class CalendarController extends Controller
{

    public function index()
    {
        return 'Index';
    }

    public function store(Request $request)
    {
        $organisation = Organisation::findOrFail($request->get('organisation_id'));
        $calendar = $organisation->calendars()->create($request->all());

        return response()->json(compact('calendar'), 201);
    }

    public function show($id)
    {
        /* Check if User got calendar with id */
        $calendar = Calendar::where('id', $id)->first();
        if (!$calendar) {
            return response()->json(['message' => 'Organisation not found'], 404);
        }
        $data = new CalendarsResource(Calendar::findOrFail($id));

        return response()->json(compact('data'));
    }

    public function update(Request $request, $id)
    {
        /* Check if User got calendar with id */
        $calendar = Calendar::where('id', $id)->first();
        if (!$calendar) {
            return response()->json(['message' => 'Organisation not found'], 404);
        }
        $calendar->update($request->all());

        return response()->json(['message' => 'Organisation update successful'], 200);
    }

    public function destroy($id)
    {
        /* Check if User got calendar with id */
        $calendar = Calendar::Find($id);
        if (!$calendar) {
            return response()->json(['message' => 'Organisation not found'], 404);
        }
        $calendar->delete();

        return response()->json(['message' => 'Organisation deleted successfully'], 204);
    }

    public function trash(Request $request)
    {
        $organisation = Organisation::find($request->get('organisation_id'));
        $calendars = Calendar::onlyTrashed()->get();
        if ($calendars){
            return response()->json($calendars);
        }
        return response()->json('Nothing in trash');
    }
}
