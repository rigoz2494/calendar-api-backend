<?php

namespace App\Http\Controllers;

use App\Models\Calendar;
use App\Models\Event;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EventController extends Controller
{
    public function store(Request $request)
    {
        $calendar = Calendar::where('id', $request->calendar_id)->first();
        if (!$calendar) {
            return response()->json(['message' => 'Calendar not found'], 404);
        }

        $event = $calendar->events()->create($request->get('event'));
        $this->fileUpload($event, $request->allFiles());

        return response()->json(['files' => $event->attached->pluck('file_path')], 201);
    }

    private function fileUpload($event, $files)
    {
        foreach ($files['event']['files'] as $file) {
            $path = Storage::putFileAs(
                'events', $file, $file->getClientOriginalName()
            );

            $data = [
                'file_name' => $file->getClientOriginalName(),
                'file_path' => storage_path('app/'. $path),
                'file_size' => $file->getSize(),
                'file_extension' => $file->getClientOriginalExtension(),
            ];

            $attached = $event->attached()->create($data);
            $data[] = $attached;
        }
        return null;
    }
}
