<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoryResource;
use App\Models\Category;

class CategoryController extends Controller
{
    public function getCategories()
    {
        $categories = CategoryResource::collection(Category::all());
        return response()->json($categories);
    }
}
