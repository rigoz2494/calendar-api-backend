<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DayResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'open_at' => $this->open_at,
            'close_at' => $this->close_at,
            'day_of_week' => $this->day_of_week,
            'organisation_id' => $this->organisation_id,
        ];
    }
}
