<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExceptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'open_at' => $this->open_at,
            'id' => $this->id,
            'close_at' => $this->close_at,
            'date' => $this->date,
            'day_id' => $this->day_id,
            'description' => $this->description,
        ];
    }
}
